//
//  Pokemon.swift
//  ResultTypePractice
//
//  Created by Mostafa Sandeed on 2/18/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

struct Pokemon: Decodable {
    let name: String?
    let imageUrl: String?
    let description: String?
    let height: Int?
    let weight: Int?
    let attack: Int?
    let type: String?
    let evolutionChain: [EvolutionChain]?
}

struct EvolutionChain: Decodable {
    let id: String?
    let name: String?
}
