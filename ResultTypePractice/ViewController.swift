//
//  ViewController.swift
//  ResultTypePractice
//
//  Created by Mostafa Sandeed on 2/18/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchData()
    }

    func fetchData() {
        
        APIClient.shared.fetchData { (result) in
            switch result {
            case .success(let pokemons):
                if let name = pokemons["1"]?.name {
                    print(name)
                }
            case .failure(let error):
                print("Failed with error: \(error)")
            }
        }
    }
}


