//
//  APIClient.swift
//  ResultTypePractice
//
//  Created by Mostafa Sandeed on 2/18/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

enum CustomError: Error {
    case networkError
    case parsingError
}

class APIClient {
    
    static let shared = APIClient()
    let baseUrl = "https://pokedex-bb36f.firebaseio.com/pokemon.json"
    
    func fetchData(completionHandler: @escaping(Result<[String:Pokemon], CustomError>) -> ()) {
        
        guard let url = URL(string: baseUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                completionHandler(.failure(.networkError))
                return
            }
            
            guard let data = data else { return }
            
            do {
                let pokemons = try JSONDecoder().decode([String: Pokemon].self, from: data)
                completionHandler(.success(pokemons))
            } catch {
                completionHandler(.failure(.parsingError))
            }
            
        }.resume()
        
    }
}
